app.showSong = function () {
  var id = parseInt(app.params['id']),
    $frame = app.$frame,
    $form = $frame.find('#form'),
    $reset  = $frame.find('#reset_'),
    $submit = $frame.find('#submit'),
    $back = $frame.find('#back')
    $tableSong = $frame.find('#table-song'),
    $msgid = $tableSong.find('#msgid'),   // 千万要防止注入
    $songname = $tableSong.find('#songname'),
    $createtime = $tableSong.find('#createtime'),
    $playlength = $tableSong.find('#playlength'),
    $plays = $tableSong.find('#plays'),
    $published = $tableSong.find('#published'),
    $audio = $frame.find('#audio'),
    $random = $frame.find('#random'),
    $down = $frame.find('#down'),
    $toggle = $frame.find('#toggle');

  _hmt.push(['_trackEvent', 'admin', 'admin-songview', 'song-'+id]);

  $back.on('click', app.backFromSong);
  $random.on('click', app.gotoRandomSong);

  $.get('song/view/' + id, function (data) {
    // 如果是字符串则parseJSON
    if (_.isString(data)) data = $.parseJSON(data);
    var msg = data['msg'],
      song = data['song'];
    msg && app.notify(msg);
    if (!song) return window.history.go(-1);  // 歌曲不存在 返回原网页
    var msgId = song['msgid'];
    $msgid.attr('value', msgId);
    $songname.attr('value', song['name']);

    var playlengthStr = song['playlength']
    $playlength.attr('value', playlengthStr);

    document.title = song['name'] + ' - ' + playlengthStr;

    if (song['published']) $published.prop('checked', true);
    $plays.attr('value', song['plays']);
    $createtime.attr('value', song['createtime'] ? (function (t) {
      var d = new Date(t),
        year = d.getFullYear(),
        month = twobits(d.getMonth() + 1),
        day = twobits(d.getDate()),
        hour = twobits(d.getHours()),
        minute = twobits(d.getMinutes()),
        second = twobits(d.getSeconds());
      return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    })(song['createtime'] * 1000) : '-');

    $audio.on('ended',function () {
      app.toggleSong(id, false);
    }).attr('preload', '') // 预加载
    // 加上.mp3后缀 格式友好
    .attr('src', 'song/down/' + msgId + '.mp3'); // 加载歌曲

    $toggle.on('click', function(){
      app.toggleSong(id);
    });
      //.find('#play').enable();

    $down.on('click',function () {
      app.downloadSong(msgId);
    }).enable();
    //$down.addClass('external')
    // .attr('target', '_blank')
    // .attr('href', 'song/down/' + msgId + '.mp3');
  
    $form.on('submit', function(e){
      e.preventDefault()
      $.post('op/song/update/' + msgId, {
        'published': $published.prop('checked') ? 'on' : '',
        'msgid': $msgid.val(),
        'name': $songname.val(),
        'playlength': $playlength.val(),
        'plays': $plays.val(),
        'createtime': $createtime.val()
      }, function(data){
        if (data['msg']) app.notify(data['msg'])
        app.reloadPage()
      })
    })
    $reset.on('click', function(){
      $form[0].reset()
    })
    $submit.on('click', function(){
      $form.submit()
    })
  });
}

app.toggleSong = function (id, flag) {
  var $frame = $('#frame'),
    $toggle = $frame.find('#toggle'),
    $audio = $frame.find('#audio'),
    audio = $audio[0];
  flag = _.isBoolean(flag) ? flag : $toggle.is('.off');
  _hmt.push(['_trackEvent', 'admin', 'admin-songtoggle', 'song-'+id]);

  if (flag) {
    $toggle.removeClass('off').addClass('on');
    audio.play();
  } else {
    $toggle.removeClass('on').addClass('off');
    audio.pause();
  }
}

app.downloadSong = function (id) {
  _hmt.push(['_trackEvent', 'admin', 'admin-songdown', 'song-'+id]);
  //window.open('song/down/' + id);
  location.href = 'song/down/' + id;
}
app.backFromSong = function () {
  _hmt.push(['_trackEvent', 'admin', 'admin-songback', 'songlist']);
  var lastHash = app.lastHash;
  if (/^#songlist/.test(lastHash)) return app.loadPage(lastHash);
  app.loadPage('#songlist');
}
